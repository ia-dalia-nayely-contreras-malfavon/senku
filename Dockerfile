
package senku;

public class Senku {
    
 static char[][]  matriz = {{' ',' ','O','O','O',' ',' '},
                            {' ',' ','O','O','O',' ',' '},
                            {'O','O','O','O','O','O','O'},
                            {'O','O','O','x','O','O','O'},
                            {'O','O','O','O','O','O','O'},
                            {' ',' ','O','O','O',' ',' '},
                            {' ',' ','O','O','O',' ',' '}};
   
    public static void main(String[] args) {
    imprimir();
    //derecho    
    matriz[3][5]='x';           
    matriz[3][4]='x';
    matriz[3][3]='O';
    imprimir();
    matriz[1][4]='x';           
    matriz[2][4]='x';
    matriz[3][4]='O';
    imprimir();
    matriz[2][6]='x';           
    matriz[2][5]='x';
    matriz[2][4]='O';
    imprimir();
    matriz[4][6]='x';           
    matriz[3][6]='x';
    matriz[2][6]='O';
    imprimir();
    matriz[2][3]='x';           
    matriz[2][4]='x';
    matriz[2][5]='O';
    imprimir();  
    matriz[2][6]='x';           
    matriz[2][5]='x';
    matriz[2][4]='O';
    imprimir();  
    
    //arriba
    matriz[2][1]='x';           
    matriz[2][2]='x';
    matriz[2][3]='O';
    imprimir();
    matriz[0][2]='x';           
    matriz[1][2]='x';
    matriz[2][2]='O';
    imprimir();
    matriz[0][4]='x';           
    matriz[0][3]='x';
    matriz[0][2]='O';
    imprimir();
    matriz[3][2]='x';           
    matriz[2][2]='x';
    matriz[1][2]='O';
    imprimir();
    matriz[0][2]='x';           
    matriz[1][2]='x';
    matriz[2][2]='O';
    imprimir();
    
    //izquierda
    System.out.print("izquierda");
    matriz[5][2]='x';           
    matriz[4][2]='x';
    matriz[3][2]='O';
    imprimir();
    matriz[4][0]='x';           
    matriz[4][1]='x';
    matriz[4][2]='O';
    imprimir();
    matriz[2][0]='x';           
    matriz[3][0]='x';
    matriz[4][0]='O';
    imprimir();
    matriz[4][3]='x';           
    matriz[4][2]='x';
    matriz[4][1]='O';
    imprimir(); 
    matriz[4][0]='x';           
    matriz[4][1]='x';
    matriz[4][2]='O';
    imprimir();
    
    //abajo
    System.out.print("abajo");
    matriz[4][5]='x';           
    matriz[4][4]='x';
    matriz[4][3]='O';
    imprimir();
    matriz[6][4]='x';           
    matriz[5][4]='x';
    matriz[4][4]='O';
    imprimir();
    matriz[6][2]='x';           
    matriz[6][3]='x';
    matriz[6][4]='O';
    imprimir();
    matriz[3][4]='x';           
    matriz[4][4]='x';
    matriz[5][4]='O';
    imprimir();
    matriz[6][4]='x';           
    matriz[5][4]='x';
    matriz[4][4]='O';
    imprimir();
    
    //casita
    System.out.print("casita");
    matriz[3][2]='x';           
    matriz[2][2]='x';
    matriz[1][2]='O';
    imprimir();
    matriz[1][2]='x';           
    matriz[1][3]='x';
    matriz[1][4]='O';
    imprimir();
    matriz[1][4]='x';           
    matriz[2][4]='x';
    matriz[3][4]='O';
    imprimir();
    matriz[3][4]='x';           
    matriz[4][4]='x';
    matriz[5][4]='O';
    imprimir();
    matriz[5][4]='x';           
    matriz[5][3]='x';
    matriz[5][2]='O';
    imprimir();
    matriz[5][2]='x';           
    matriz[4][2]='x';
    matriz[3][2]='O';
    imprimir();
    
    //T
    System.out.print("T");
    matriz[3][3]='x';           
    matriz[2][3]='x';
    matriz[1][3]='O';
    imprimir();
    matriz[3][1]='x';           
    matriz[3][2]='x';
    matriz[3][3]='O';
    imprimir();
    matriz[4][3]='x';           
    matriz[3][3]='x';
    matriz[2][3]='O';
    imprimir();
    matriz[1][3]='x';           
    matriz[2][3]='x';
    matriz[3][3]='O';
    imprimir();
    
    
    
}
public static void imprimir(){
    for(int i = 0; i<7;i++){
    System.out.println("");
        for(int j=0; j<7; j++){
        System.out.print(matriz[i][j]);
        }
    }
 System.out.println("");             
}


}